package com.dlse.taller.clientservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dlse.taller.clientservice.service.ClientService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(value = "/clients", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ClientController {
	
	private final ClientService service;
	
	private static final String DEBUG_MSG = "request: {}";
	
	@GetMapping
	public ResponseEntity getPlan(){
		service.findByKey("");
		return null;
	}
	
	@PostMapping
	public ResponseEntity createPlan() {
		service.create(null);
		return null;
	}

}
