package com.dlse.taller.clientservice.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Client {
	
	private Long id;
    
	private String key;
	
    private String name;
	
    private String DoB;
	
    private String address;
	
    private Long membershipFK;

}
