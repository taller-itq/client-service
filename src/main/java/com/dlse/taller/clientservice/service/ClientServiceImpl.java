package com.dlse.taller.clientservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dlse.taller.clientservice.domain.entity.ClientEntity;
import com.dlse.taller.clientservice.domain.repository.ClientRepository;
import com.dlse.taller.clientservice.model.Client;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ClientServiceImpl implements ClientService {

	private final ClientRepository repository;
	
	private static final String WARNING_MSG = "Warning: {}";

	@Override
	public Client create(Client client) {
		ClientEntity entity = repository.save(null);
		return null;
	}

	@Override
	public Client findByKey(String key) {
		ClientEntity entity = repository.findByKey(null);
		return null;
	}
}
