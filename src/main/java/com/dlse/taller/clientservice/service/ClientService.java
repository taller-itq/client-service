package com.dlse.taller.clientservice.service;

import com.dlse.taller.clientservice.model.Client;

public interface ClientService {
	
	public Client create(Client client);
	
	public Client findByKey(String key);

}
