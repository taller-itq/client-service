package com.dlse.taller.clientservice.domain.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.dlse.taller.clientservice.domain.entity.ClientEntity;

@Repository
public interface ClientDAO extends CrudRepository<ClientEntity, Long>{
	public ClientEntity findByKey(String key);
}
