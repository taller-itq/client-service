package com.dlse.taller.clientservice.domain.entity;

import java.util.Date;

import org.hibernate.validator.constraints.UUID;

import com.dlse.taller.clientservice.util.DBHelper;

import jakarta.annotation.Nullable;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(
        name = "products",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "key")
        }
)
@EntityListeners(DBHelper.class)
public class ClientEntity {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Nullable
    private Long id;
	
	@Nullable
    @Column(updatable = false, nullable = false, unique = true)
    @UUID
    private String key;
	
	@NotEmpty
    private String name;
	
	@NotEmpty
    private String DoB;
	
	@NotEmpty
    private String address;
	
	@NotEmpty
	@Column(name = "membership_fk")
    private Long membershipFK;

}
