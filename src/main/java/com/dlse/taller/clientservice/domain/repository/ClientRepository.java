package com.dlse.taller.clientservice.domain.repository;

import com.dlse.taller.clientservice.domain.entity.ClientEntity;

public interface ClientRepository {
	
	public ClientEntity save(ClientEntity plan);
	
	public ClientEntity findByKey(String key);

}
