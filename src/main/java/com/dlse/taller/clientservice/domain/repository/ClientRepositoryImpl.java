package com.dlse.taller.clientservice.domain.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dlse.taller.clientservice.domain.dao.ClientDAO;
import com.dlse.taller.clientservice.domain.entity.ClientEntity;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ClientRepositoryImpl implements ClientRepository {
	
	private final ClientDAO dao;
	
    private static final String ERROR_MSG = "error: {} {} {}";
	
	@Override
	public ClientEntity save(ClientEntity client) {
		return dao.save(client);
	}

	@Override
	public ClientEntity findByKey(String key) {
		return dao.findByKey(key);
	}

}
