package com.dlse.taller.clientservice.util;

import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.dlse.taller.clientservice.domain.entity.ClientEntity;

import jakarta.persistence.PrePersist;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class DBHelper {

    /**
     * automatic property set before any database persistence
     */
    @PrePersist
    public void setKeyPrePersist(ClientEntity client) {
        if (StringUtils.isBlank(client.getKey())) {
            client.setKey(null);
        }
    }

}
